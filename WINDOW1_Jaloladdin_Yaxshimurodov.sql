WITH CustomerSales AS (
    SELECT
        c.cust_id AS customer_id,
        CONCAT(c.cust_first_name, ' ', c.cust_last_name) AS customer_name,
        s.channel_id,
        SUM(s.amount_sold) AS total_sales,
        RANK() OVER (ORDER BY SUM(s.amount_sold) DESC) AS sales_rank
    FROM
        sh.sales s
    JOIN
        sh.customers c ON s.cust_id = c.cust_id
    JOIN
        sh.times t ON s.time_id = t.time_id    
    WHERE
        t.calendar_year IN (1998, 1999, 2001)
    GROUP BY
        c.cust_id, c.cust_first_name, c.cust_last_name, s.channel_id
)
SELECT
    cs.customer_id,
    cs.customer_name,
    cs.channel_id,
    ch.channel_desc,
    ch.channel_class,
    ROUND(cs.total_sales, 2) AS total_sales
FROM
    CustomerSales cs
JOIN
    sh.channels ch ON cs.channel_id = ch.channel_id
WHERE
    cs.sales_rank <= 300
ORDER BY
    cs.total_sales DESC, cs.customer_id, cs.channel_id;
