WITH SubcategorySales AS (
    SELECT
        p.prod_subcategory,
        t.calendar_year AS current_year,
        SUM(s.amount_sold) AS total_sales_current_year,
        LAG(SUM(s.amount_sold)) OVER (PARTITION BY p.prod_subcategory ORDER BY t.calendar_year) AS total_sales_previous_year
    FROM
        sh.sales s
    JOIN
        sh.products p ON s.prod_id = p.prod_id
    JOIN
        sh.times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, t.calendar_year
)
SELECT DISTINCT
    prod_subcategory
FROM
    SubcategorySales
WHERE
    total_sales_current_year > COALESCE(total_sales_previous_year, 0);
